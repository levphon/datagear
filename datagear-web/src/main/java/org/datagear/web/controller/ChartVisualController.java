/*
 * Copyright 2018-present datagear.tech
 *
 * This file is part of DataGear.
 *
 * DataGear is free software: you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 *
 * DataGear is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with DataGear.
 * If not, see <https://www.gnu.org/licenses/>.
 */

package org.datagear.web.controller;

import org.datagear.analysis.DashboardResult;
import org.datagear.analysis.TplChartWidgetResManager;
import org.datagear.analysis.TplDashboardWidgetResManager;
import org.datagear.analysis.support.ErrorMessageDashboardResult;
import org.datagear.analysis.support.html.*;
import org.datagear.management.domain.Authorization;
import org.datagear.management.domain.ChartShareSet;
import org.datagear.management.domain.HtmlChartWidgetEntity;
import org.datagear.management.domain.User;
import org.datagear.management.service.ChartShareSetService;
import org.datagear.management.service.HtmlChartWidgetEntityService;
import org.datagear.management.service.HtmlChartWidgetEntityService.ChartWidgetSourceContext;
import org.datagear.management.service.PermissionDeniedException;
import org.datagear.util.FileUtil;
import org.datagear.util.IOUtil;
import org.datagear.util.StringUtil;
import org.datagear.web.config.ApplicationProperties;
import org.datagear.web.config.CoreConfigSupport;
import org.datagear.web.util.OperationMessage;
import org.datagear.web.util.SessionDashboardInfoSupport.DashboardInfo;
import org.datagear.web.util.WebUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.ServletContextAware;
import org.springframework.web.context.request.WebRequest;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.*;
import java.util.HashMap;
import java.util.Map;

/**
 * 图表展示控制器。
 *
 * @author datagear@163.com
 *
 */
@Controller
@RequestMapping(ChartVisualController.PATH_PREFIX)
public class ChartVisualController extends AbstractDataAnalysisController implements ServletContextAware
{
	/** 展示页路径前缀 */
	public static final String PATH_PREFIX = "/cv";

	public static final String CHART_SHOW_AUTH_PARAM_NAME = "name";

	/** 加载看板图表参数：图表部件ID */
	public static final String LOAD_CHART_PARAM_CHART_WIDGET_ID = "chartWidgetId";

	@Autowired
	private HtmlChartWidgetEntityService htmlChartWidgetEntityService;

	@Autowired
	private ChartShareSetService chartShareSetService;

	@Autowired
	private HtmlTplDashboardWidgetHtmlRenderer htmlTplDashboardWidgetHtmlRenderer;

	@Autowired
	private TplDashboardWidgetResManager tplDashboardWidgetResManager;

	@Autowired
	private ApplicationProperties applicationProperties;

	@Autowired
	@Qualifier(CoreConfigSupport.NAME_CHART_GLOBAL_RES_ROOT_DIRECTORY)
	private File chartGlobalResRootDirectory;

	private ServletContext servletContext;

	public ChartVisualController()
	{
		super();
	}

	public ServletContext getServletContext()
	{
		return servletContext;
	}

	@Override
	public void setServletContext(ServletContext servletContext)
	{
		this.servletContext = servletContext;
	}

	/**
	 * 看板展示认证页面。
	 *
	 * @param request
	 * @param response
	 * @param model
	 * @param id
	 *            看板ID
	 * @param name
	 *            重定向的看板资源名，允许为{@code null}，格式为："abc.html"、"def/def.js"、"detail/detail.html?param=0"
	 * @throws Exception
	 */
	@RequestMapping({ "/auth/{id}", "/auth/{id}/**" })
	public String showAuth(HttpServletRequest request, HttpServletResponse response, Model model,
						   @PathVariable("id") String id,
						   @RequestParam(value = CHART_SHOW_AUTH_PARAM_NAME, required = false) String name) throws Exception
	{
		User user = getCurrentUser();
		setFormAction(model, "auth", "authcheck");

		HtmlChartWidgetEntity chartWidget = this.htmlChartWidgetEntityService
				.getById(user, id);

		if (chartWidget == null)
			throw new RecordNotFoundException();

		if (!StringUtil.isEmpty(name))
			name = WebUtils.decodeURL(name);

		String redirectPath = WebUtils.getContextPath(request) + resolveShowPath(request, id);
		String submitPath = resolveAuthSubmitPath(request);

		if (!StringUtil.isEmpty(name))
		{
			redirectPath = FileUtil.concatPath(redirectPath, name, FileUtil.PATH_SEPARATOR_SLASH, false);
		}

		redirectPath = addSessionIdParamIfNeed(redirectPath, request);
		submitPath = addSessionIdParamIfNeed(submitPath, request);

		boolean authed = isShowAuthed(request, user, chartWidget);

		Map<String, Object> authModel = new HashMap<String, Object>();
		authModel.put("id", id);
		authModel.put("name", (name == null ? "" : name));
		authModel.put("redirectPath", redirectPath);
		authModel.put("authed", authed);
		authModel.put("dashboardNameMask", StringUtil.mask(chartWidget.getName(), 2, 2, 6));

		setFormModel(model, authModel);
		model.addAttribute("authed", authed);
		model.addAttribute("submitPath", submitPath);
		model.addAttribute("redirectPath", redirectPath);

		return "/chart/chart_show_auth";
	}

	/**
	 * 看板展示认证校验。
	 *
	 * @param request
	 * @param response
	 * @param model
	 * @param form
	 * @throws Exception
	 */
	@RequestMapping(value = "/authcheck", produces = CONTENT_TYPE_JSON)
	@ResponseBody
	public ResponseEntity<OperationMessage> showAuthCheck(HttpServletRequest request, HttpServletResponse response,
														  Model model, @RequestBody DashboardVisualController.ShowAuthCheckForm form) throws Exception
	{
		if (isEmpty(form.getId()))
			throw new IllegalInputException();

		User user = getCurrentUser();
		HtmlChartWidgetEntity dashboardWidget = getByIdForView(this.htmlChartWidgetEntityService, user,
				form.getId());

		String password = (StringUtil.isEmpty(form.getPassword()) ? "" : form.getPassword());

		ResponseEntity<OperationMessage> responseEntity = null;

		String dashboardWidgetId = dashboardWidget.getId();
		ChartShareSet chartShareSet = this.chartShareSetService.getById(dashboardWidgetId);

		if (chartShareSet == null || !chartShareSet.isEnablePassword())
		{
			responseEntity = optSuccessDataResponseEntity(request,
					DashboardVisualController.ShowAuthCheckResponse.valueOf(DashboardVisualController.ShowAuthCheckResponse.TYPE_SUCCESS));
		}
		else
		{
			DashboardVisualController.DashboardShowAuthCheckManager manager = getSessionDashboardShowAuthCheckManager(request);

			if (manager.isAuthDenied(dashboardWidgetId))
			{
				responseEntity = optSuccessDataResponseEntity(request, DashboardVisualController.ShowAuthCheckResponse
						.valueOf(DashboardVisualController.ShowAuthCheckResponse.TYPE_DENY, manager.getAuthFailThreshold(), 0));
			}
			else if (password.equals(chartShareSet.getPassword()))
			{
				manager.setAuthed(dashboardWidgetId, true);
				responseEntity = optSuccessDataResponseEntity(request,
						DashboardVisualController.ShowAuthCheckResponse.valueOf(DashboardVisualController.ShowAuthCheckResponse.TYPE_SUCCESS));
			}
			else
			{
				manager.setAuthed(dashboardWidgetId, false);
				int authRemain = manager.authRemain(dashboardWidgetId);

				if (authRemain > 0)
				{
					responseEntity = optSuccessDataResponseEntity(request, DashboardVisualController.ShowAuthCheckResponse
							.valueOf(DashboardVisualController.ShowAuthCheckResponse.TYPE_FAIL, manager.getAuthFailThreshold(), authRemain));
				}
				else
				{
					responseEntity = optSuccessDataResponseEntity(request, DashboardVisualController.ShowAuthCheckResponse
							.valueOf(DashboardVisualController.ShowAuthCheckResponse.TYPE_DENY, manager.getAuthFailThreshold(), 0));
				}
			}

			setSessionDashboardShowAuthCheckManager(request, manager);
		}

		return responseEntity;
	}

	/**
	 * 看板展示请求是否已通过密码验证。
	 *
	 * @param request
	 * @param user
	 * @param chartWidget
	 * @return
	 */
	protected boolean isShowAuthed(HttpServletRequest request, User user, HtmlChartWidgetEntity chartWidget)
	{
		// 有编辑权限，无需认证
		if (Authorization.canEdit(chartWidget.getDataPermission()))
			return true;

		ChartShareSet chartShareSet = this.chartShareSetService.getById(chartWidget.getId());

		if (chartShareSet == null || !chartShareSet.isEnablePassword())
			return true;

		if (chartShareSet.isAnonymousPassword() && !user.isAnonymous())
			return true;

		DashboardVisualController.DashboardShowAuthCheckManager manager = getSessionDashboardShowAuthCheckManager(request);
		return manager.isAuthed(chartWidget.getId());
	}

	protected String buildShowAuthUrlForShowRequest(HttpServletRequest request, HttpServletResponse response, String id,
													String resName) throws Exception
	{
		resName = appendRequestQueryString((resName == null ? "" : resName), request);
		String authPath = WebUtils.getContextPath(request) + resolveAuthPath(request, id);
		authPath = addSessionIdParamIfNeed(authPath, request);
		authPath = addSafeSessionParamIfNeed(authPath, request);

		if (!StringUtil.isEmpty(resName))
		{
			authPath = WebUtils.addUrlParam(authPath, CHART_SHOW_AUTH_PARAM_NAME, WebUtils.encodeURL(resName));
		}

		return authPath;
	}

	/**
	 * 获取会话中的{@linkplain DashboardVisualController.DashboardShowAuthCheckManager}。
	 * <p>
	 * 如果修改了获取的{@linkplain DashboardVisualController.DashboardShowAuthCheckManager}的状态，应在修改之后调用{@linkplain #setSessionDashboardShowAuthCheckManager(HttpServletRequest, DashboardVisualController.DashboardShowAuthCheckManager)}，
	 * 以为可能扩展的分布式会话提供支持。
	 * </p>
	 *
	 * @param request
	 * @return 非{@code null}
	 */
	protected DashboardVisualController.DashboardShowAuthCheckManager getSessionDashboardShowAuthCheckManager(HttpServletRequest request)
	{
		HttpSession session = request.getSession();

		DashboardVisualController.DashboardShowAuthCheckManager manager = null;

		synchronized (session)
		{
			manager = (DashboardVisualController.DashboardShowAuthCheckManager) session
					.getAttribute(DashboardVisualController.DashboardShowAuthCheckManager.class.getName());

			if (manager == null)
			{
				manager = new DashboardVisualController.DashboardShowAuthCheckManager(
						this.applicationProperties.getDashboardSharePsdAuthFailThreshold(),
						this.applicationProperties.getDashboardSharePsdAuthFailPastMinutes() * 60 * 1000);

				session.setAttribute(DashboardVisualController.DashboardShowAuthCheckManager.class.getName(), manager);
			}
		}

		return manager;
	}

	/**
	 * 设置会话中的{@linkplain DashboardVisualController.DashboardShowAuthCheckManager}。
	 *
	 * @param request
	 * @param manager
	 */
	protected void setSessionDashboardShowAuthCheckManager(HttpServletRequest request,
														   DashboardVisualController.DashboardShowAuthCheckManager manager)
	{
		HttpSession session = request.getSession();
		session.setAttribute(DashboardVisualController.DashboardShowAuthCheckManager.class.getName(), manager);
	}

	/**
	 * 展示图表。
	 *
	 * @param request
	 * @param response
	 * @param model
	 * @param id
	 * @throws Exception
	 */
	@RequestMapping({ "/{id}/", "/{id}" })
	public void show(HttpServletRequest request, HttpServletResponse response, org.springframework.ui.Model model,
			@PathVariable("id") String id) throws Exception
	{
		String requestPath = resolvePathAfter(request, "");
		String correctPath = WebUtils.getContextPath(request) + resolveShowPath(request, id);

		// 如果是"/{id}"请求，则应跳转到"/{id}/"，因为图表展示页内的超链接使用的都是相对路径，
		// 如果末尾不加"/"，将会导致这些超链接路径错误
		if (requestPath.indexOf(correctPath) < 0)
		{
			String redirectPath = correctPath;
			redirectPath = addSessionIdParamIfNeed(redirectPath, request);
			redirectPath = appendRequestQueryString(redirectPath, request);
			response.sendRedirect(redirectPath);
		}
		else
		{
			User user = getCurrentUser();
			HtmlChartWidgetEntity chartWidget = getHtmlTplChartWidgetEntityForShow(request, user, id);

			if (!isShowAuthed(request, user, chartWidget))
			{
				String authPath = buildShowAuthUrlForShowRequest(request, response, id, "");
				response.sendRedirect(authPath);
				return;
			}

//			String firstTemplate = chartWidget.getFirstTemplate();
//
//			// 如果首页模板是在嵌套路径下，则应重定向到具体路径，避免页面内以相对路径引用的资源找不到
//			int subPathSlashIdx = firstTemplate.indexOf(FileUtil.PATH_SEPARATOR_SLASH);
//			if (subPathSlashIdx > 0 && subPathSlashIdx < firstTemplate.length() - 1)
//			{
//				String redirectPath = correctPath + WebUtils.encodePathURL(firstTemplate);
//				redirectPath = addSessionIdParamIfNeed(redirectPath, request);
//				redirectPath = appendRequestQueryString(redirectPath, request);
//
//				response.sendRedirect(redirectPath);
//			}
//			else
//			{
				showChart(request, response, model, user, chartWidget);
//			}
		}
	}

	/**
	 * 获取看板展示时使用的看板部件。
	 *
	 * @param request
	 * @param user
	 * @param id
	 * @return
	 * @throws PermissionDeniedException
	 *             没有权限时
	 * @throws RecordNotFoundException
	 *             记录不存在时
	 */
	protected HtmlChartWidgetEntity getHtmlTplChartWidgetEntityForShow(HttpServletRequest request, User user,
																				  String id) throws PermissionDeniedException, RecordNotFoundException
	{
		if (StringUtil.isEmpty(id))
			throw new IllegalInputException();

		HtmlChartWidgetEntity chartWidget = this.htmlChartWidgetEntityService
				.getById(user, id);

		if (chartWidget == null)
			throw new RecordNotFoundException();

		return chartWidget;
	}

	protected boolean isChartShowForEditParam(HttpServletRequest request)
	{
		String editTemplate = request.getParameter(DASHBOARD_SHOW_PARAM_EDIT_TEMPLATE);
		// 有编辑模板请求参数
		return StringUtil.toBoolean(editTemplate);
	}

	protected boolean isChartShowForEditRequest(HttpServletRequest request,
												HtmlChartWidgetEntity chartWidget)
	{
		// 有编辑模板请求参数、且有编辑权限
		return (isChartShowForEditParam(request) && Authorization.canEdit(chartWidget.getDataPermission()));
	}

	/**
	 * 加载展示图表的资源。
	 *
	 * @param request
	 * @param response
	 * @param webRequest
	 * @param model
	 * @param id
	 * @throws Exception
	 */
	@RequestMapping("/{id}/**")
	public void showResource(HttpServletRequest request, HttpServletResponse response, WebRequest webRequest,
			org.springframework.ui.Model model, @PathVariable("id") String id) throws Exception
	{
		User user = getCurrentUser();
		HtmlChartWidgetEntity chartWidget = this.htmlChartWidgetEntityService.getById(user, id);

		String resName = resolvePathAfter(request, resolveShowPath(request, id));

		if (isEmpty(resName))
		{
			showChart(request, response, model, user, chartWidget);
		}

		if (!isShowAuthed(request, user, chartWidget))
		{
			String authPath = buildShowAuthUrlForShowRequest(request, response, id, resName);
			response.sendRedirect(authPath);
			return;
		}

		// 处理可能的中文资源名
		resName = WebUtils.decodeURL(resName);

		long lastModified = this.tplDashboardWidgetResManager.lastModified(id, resName);
		if (webRequest.checkNotModified(lastModified))
			return;

		setContentTypeByName(request, response, servletContext, resName);
		setCacheControlNoCache(response);

		InputStream in = this.tplDashboardWidgetResManager.getInputStream(id, resName);
		OutputStream out = response.getOutputStream();

		try
		{
			IOUtil.write(in, out);
		}
		finally
		{
			IOUtil.close(in);
		}

	}


	/**
	 * 展示数据。
	 *
	 * @param request
	 * @param response
	 * @param model
	 * @param id
	 * @throws Exception
	 */
	@RequestMapping(value = "/data", produces = CONTENT_TYPE_JSON)
	@ResponseBody
	public ErrorMessageDashboardResult showData(HttpServletRequest request, HttpServletResponse response,
			org.springframework.ui.Model model, @RequestBody DashboardQueryForm form) throws Exception
	{
		// 此处获取ChartWidget不再需要权限控制，应显式移除线程变量
		ChartWidgetSourceContext.remove();

		DashboardResult dashboardResult = getDashboardResult(request, response, form,
				this.htmlTplDashboardWidgetHtmlRenderer);

		return new ErrorMessageDashboardResult(dashboardResult, true);
	}

	/**
	 * 心跳。
	 * <p>
	 * 图表展示页面有停留较长时间再操作的场景，此时可能会因为会话超时导致操作失败，所以这里添加心跳请求，避免会话超时。
	 * </p>
	 *
	 * @param request
	 * @param response
	 * @param dashboardId
	 * @return
	 * @throws Throwable
	 */
	@RequestMapping(value = HEARTBEAT_TAIL_URL, produces = CONTENT_TYPE_JSON)
	@ResponseBody
	public Map<String, Object> heartbeat(HttpServletRequest request, HttpServletResponse response,
			@RequestParam(HEARTBEAT_PARAM_DASHBOARD_ID) String dashboardId) throws Throwable
	{
		return super.handleHeartbeat(request, response, dashboardId);
	}

	/**
	 * 图表展示页卸载。
	 * <p>
	 * 图表展示页面关闭后，应卸载后台数据。
	 * </p>
	 *
	 * @param request
	 * @param response
	 * @param dashboardId
	 * @return
	 * @throws Throwable
	 */
	@RequestMapping(value = UNLOAD_TAIL_URL, produces = CONTENT_TYPE_JSON)
	@ResponseBody
	public Map<String, Object> unloadDashboard(HttpServletRequest request, HttpServletResponse response,
			@RequestParam(UNLOAD_PARAM_DASHBOARD_ID) String dashboardId) throws Throwable
	{
		return super.handleUnloadDashboard(request, response, dashboardId);
	}

	/**
	 * 展示图表。
	 *
	 * @param request
	 * @param response
	 * @param model
	 * @param id
	 * @throws Exception
	 */
	protected void showChart(HttpServletRequest request, HttpServletResponse response,
			org.springframework.ui.Model model, User user, HtmlChartWidgetEntity chart) throws Exception
	{
		if (chart == null)
			throw new RecordNotFoundException();

		Reader templateIn = null;
		Writer out = null;

		ChartWidgetSourceContext.set(new ChartWidgetSourceContext(user));

		try
		{
			String id = chart.getId();
			String htmlTitle = chart.getName();
			HtmlTplDashboardWidget dashboardWidget = buildHtmlTplDashboardWidget(id);

			// 图表展示页面应禁用异步加载功能，避免越权访问隐患
			String htmlAttr = this.htmlTplDashboardWidgetHtmlRenderer.getAttrNameLoadableChartWidgets() + "=\""
					+ LoadableChartWidgets.PATTERN_NONE + "\"";
			String simpleTemplate = this.htmlTplDashboardWidgetHtmlRenderer.simpleTemplateContent(new String[] { id },
					htmlAttr, IOUtil.CHARSET_UTF_8, htmlTitle,
					this.htmlTplDashboardWidgetHtmlRenderer.getDashboardStyleName(), "",
					"dg-chart-for-show-chart " + this.htmlTplDashboardWidgetHtmlRenderer.getChartStyleName(),
					"dg-chart-disable-setting=\"false\"");
			templateIn = IOUtil.getReader(simpleTemplate);

			String responseEncoding = dashboardWidget.getTemplateEncoding();
			response.setCharacterEncoding(responseEncoding);
			response.setContentType(CONTENT_TYPE_HTML);
			out = IOUtil.getBufferedWriter(response.getWriter());

			HtmlTitleHandler htmlTitleHandler = getShowChartHtmlTitleHandler(request, response, user, chart);
			HtmlTplDashboardRenderContext renderContext = createRenderContext(request, response,
					dashboardWidget.getFirstTemplate(), out, createWebContext(request),
					buildWebHtmlTplDashboardImportBuilderForShow(request), htmlTitleHandler);
			renderContext.setTemplateReader(templateIn);
			renderContext.setTemplateLastModified(HtmlTplDashboardRenderContext.TEMPLATE_LAST_MODIFIED_NONE);

			HtmlTplDashboard dashboard = dashboardWidget.render(renderContext);
			getSessionDashboardInfoSupport().setDashboardInfo(request, new DashboardInfo(dashboard, false));
		}
		finally
		{
			IOUtil.close(templateIn);
			IOUtil.close(out);
			ChartWidgetSourceContext.remove();
		}
	}

	protected HtmlTitleHandler getShowChartHtmlTitleHandler(HttpServletRequest request, HttpServletResponse response,
			User user, HtmlChartWidgetEntity chart) throws Exception
	{
		return new DefaultHtmlTitleHandler(
				getMessage(request, "chart.show.htmlTitleSuffix", getMessage(request, "app.name")));
	}

	@Override
	protected boolean isDashboardThemeAuto(HttpServletRequest request, String theme)
	{
		// 由于图表展示无法自定义页面样式，因此，参数未指定主题时，也应自动匹配系统主题
		return (theme == null || super.isDashboardThemeAuto(request, theme));
	}

	protected HtmlTplDashboardWidget buildHtmlTplDashboardWidget(String chartId)
	{
		return new HtmlTplDashboardWidget(chartId, "index.html", this.htmlTplDashboardWidgetHtmlRenderer,
				this.tplDashboardWidgetResManager);
	}

	protected WebContext createWebContext(HttpServletRequest request)
	{
		WebContext webContext = createInitWebContext(request);

		addUpdateDataValue(request, webContext, resolveDataPath(request));
		addLoadChartValue(request, webContext, resolveLoadChartPath(request));
		addHeartBeatValue(request, webContext, resolveHeartbeatPath(request));
		addUnloadValue(request, webContext, resolveUnloadPath(request));
		addPluginResUrlPrefixValue(request, webContext, resolvePluginResPathPrefix(request));

		return webContext;
	}

	/**
	 * 解析展示路径。
	 *
	 * @param request
	 * @param chartId
	 * @return
	 */
	protected String resolveShowPath(HttpServletRequest request, String chartId)
	{
		return PATH_PREFIX + "/" + chartId + "/";
	}

	/**
	 * 解析加载数据路径。
	 *
	 * @param request
	 * @return
	 */
	protected String resolveDataPath(HttpServletRequest request)
	{
		return PATH_PREFIX + "/data";
	}

	/**
	 * 解析加载图表路径。
	 *
	 * @param request
	 * @return
	 */
	protected String resolveLoadChartPath(HttpServletRequest request)
	{
		return PATH_PREFIX + "/loadChart";
	}

	/**
	 * 解析心跳路径。
	 *
	 * @param request
	 * @return
	 */
	protected String resolveHeartbeatPath(HttpServletRequest request)
	{
		return PATH_PREFIX + HEARTBEAT_TAIL_URL;
	}

	/**
	 * 解析卸载看板路径。
	 *
	 * @param request
	 * @return
	 */
	protected String resolveUnloadPath(HttpServletRequest request)
	{
		return PATH_PREFIX + UNLOAD_TAIL_URL;
	}

	/**
	 * 解析认证路径。
	 *
	 * @param request
	 * @param chartId
	 * @return
	 */
	protected String resolveAuthPath(HttpServletRequest request, String chartId)
	{
		return PATH_PREFIX + "/auth/" + chartId + "/";
	}

	/**
	 * 解析认证校验路径。
	 *
	 * @param request
	 * @return
	 */
	protected String resolveAuthSubmitPath(HttpServletRequest request)
	{
		return PATH_PREFIX + "/authcheck";
	}

	public static class ShowAuthCheckForm implements ControllerForm
	{
		private static final long serialVersionUID = 1L;

		private String id;

		private String password;

		public ShowAuthCheckForm()
		{
			super();
		}

		public String getId()
		{
			return id;
		}

		public void setId(String id)
		{
			this.id = id;
		}

		public String getPassword()
		{
			return password;
		}

		public void setPassword(String password)
		{
			this.password = password;
		}
	}

	public static class ShowAuthCheckResponse implements Serializable
	{
		private static final long serialVersionUID = 1L;

		/** 验证通过 */
		public static final String TYPE_SUCCESS = "success";

		/** 验证未通过 */
		public static final String TYPE_FAIL = "fail";

		/** 验证拒绝 */
		public static final String TYPE_DENY = "deny";

		/** 校验结果类型 */
		private String type;

		private int authFailThreshold = -1;

		/** 验证剩余次数 */
		private int authRemain = -1;

		public ShowAuthCheckResponse(String type)
		{
			super();
			this.type = type;
		}

		public ShowAuthCheckResponse(String type, int authFailThreshold, int authRemain)
		{
			super();
			this.type = type;
			this.authFailThreshold = authFailThreshold;
			this.authRemain = authRemain;
		}

		public String getType()
		{
			return type;
		}

		public void setType(String type)
		{
			this.type = type;
		}

		public int getAuthFailThreshold()
		{
			return authFailThreshold;
		}

		public void setAuthFailThreshold(int authFailThreshold)
		{
			this.authFailThreshold = authFailThreshold;
		}

		public int getAuthRemain()
		{
			return authRemain;
		}

		public void setAuthRemain(int authRemain)
		{
			this.authRemain = authRemain;
		}

		public static DashboardVisualController.ShowAuthCheckResponse valueOf(String type)
		{
			return new DashboardVisualController.ShowAuthCheckResponse(type);
		}

		public static DashboardVisualController.ShowAuthCheckResponse valueOf(String type, int authFailThreshold, int authRemain)
		{
			return new DashboardVisualController.ShowAuthCheckResponse(type, authFailThreshold, authRemain);
		}
	}

}
