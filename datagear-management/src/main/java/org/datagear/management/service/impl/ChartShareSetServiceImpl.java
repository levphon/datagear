/*
 * Copyright 2018-present datagear.tech
 *
 * This file is part of DataGear.
 *
 * DataGear is free software: you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 *
 * DataGear is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with DataGear.
 * If not, see <https://www.gnu.org/licenses/>.
 */

package org.datagear.management.service.impl;

import org.apache.ibatis.session.SqlSessionFactory;
import org.datagear.management.domain.ChartShareSet;
import org.datagear.management.service.ChartShareSetService;
import org.datagear.management.util.dialect.MbSqlDialect;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.security.crypto.encrypt.TextEncryptor;

import java.util.Map;

/**
 * {@linkplain ChartShareSetService}实现类。
 *
 * @author datagear@163.com
 *
 */
public class ChartShareSetServiceImpl extends AbstractMybatisEntityService<String, ChartShareSet>
		implements ChartShareSetService
{
	protected static final String SQL_NAMESPACE = ChartShareSet.class.getName();

	private TextEncryptor textEncryptor = null;

	public ChartShareSetServiceImpl()
	{
		super();
	}

	public ChartShareSetServiceImpl(SqlSessionFactory sqlSessionFactory, MbSqlDialect dialect)
	{
		super(sqlSessionFactory, dialect);
	}

	public ChartShareSetServiceImpl(SqlSessionTemplate sqlSessionTemplate, MbSqlDialect dialect)
	{
		super(sqlSessionTemplate, dialect);
	}

	public TextEncryptor getTextEncryptor()
	{
		return textEncryptor;
	}

	public void setTextEncryptor(TextEncryptor textEncryptor)
	{
		this.textEncryptor = textEncryptor;
	}

	@Override
	public void save(ChartShareSet entity)
	{
		if (!super.update(entity))
			super.add(entity);
	}

	@Override
	protected void add(ChartShareSet entity, Map<String, Object> params)
	{
		if (this.textEncryptor != null)
		{
			entity = entity.clone();
			entity.setPassword(this.textEncryptor.encrypt(entity.getPassword()));
		}

		super.add(entity, params);
	}

	@Override
	protected boolean update(ChartShareSet entity, Map<String, Object> params)
	{
		if (this.textEncryptor != null)
		{
			entity = entity.clone();
			entity.setPassword(this.textEncryptor.encrypt(entity.getPassword()));
		}

		return super.update(entity, params);
	}

	@Override
	protected ChartShareSet getByIdFromDB(String id, Map<String, Object> params)
	{
        ChartShareSet entity = super.getByIdFromDB(id, params);

		if (this.textEncryptor != null && entity != null)
			entity.setPassword(this.textEncryptor.decrypt(entity.getPassword()));

		return entity;
	}

	@Override
	protected String getSqlNamespace()
	{
		return SQL_NAMESPACE;
	}
}
